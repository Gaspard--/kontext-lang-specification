# Intro

Kontext is an interpreted language, designed to build compiled binary.
The core idea of the language is that it produces an unevaluated expression which will become a binary.
The goals we want to prioritize are:
- Extensible, addaptable to domain usage
- Expressive, allows to really respect DRY (Do Not Repeat Yourself)
- Controlled, avoid hidden processes such as garbage collection etc. which could cause unexpected stalls

## Primitives

The compiler supports the following raw types (to be extended):
- `u[bitcount]`, `i[bitcount]`, `f`, `d`. The bitcount can be ommited in which case it's 32.
- `null`: unit value which can be instanciated. It does not contain anything and means false. It's type is `Null`. To represent a true value, one should use `~null`.
- `_`: a special primitive that is everything, meaning assignements have no effect. It's type is `Any`. `~_` is an impossible value, which can be used for variables that shouldn't be used as is (ex: uninitialized). Note that _ does not include references.

```rust
1u28; // 1 encoded in an unsigned int of 28. The compiler will probably use 32 bits anyway, but it might exploit the extra 4 bits.
10i16; // 10 encoded in a signed 2-complement 16 bit int
13.5f;
```

## Unevaluated expression

Do declare an unevaluated expression, use the `?` operator.

```rust
1i?;
```

To evaluate an unevaluated expression, use the `!` operator.

```rust
1i?!; // 1i
```

## Named variables and references

To create a variable use the `let` prefix.
It returns an immediatly assignable reference.
It's possible to recreate a variable, potentially changing it's type.
By default, a variable has the `~_` value, making it unusable.

```rust
let a = ~_; // will cause an error if a is used, since a is of type Never
let a; // identical to previous declaration
let a = null; // ok, a is now of type ref Null.
```

A named variable is of type `ref` the variable's type.
Droping a ref can be done with the + prefix.

```rust
let cat = 3u;
cat; // ref to U
+cat; // 3u
```

### Reference modifiers

References can be tagged with modifiers. The most common example is `const` prefix, which renders a variable unchangeable (but not unassignable, see assignement).

```rust
const let a = 1i; // is actually const (let a = 1i)
let b = 1i;
const b; // same thing
let d = 3i
let c = d;
const c; // renders both c and d const
let e = +d; // using + gets rid of the reference, so e is a mutable copy of d
```

### Variable and value assignement

Assignement is done using the `=`, `<-` and `->`  operator.
- `=` assigns the type and value of the right hand side to the left hand side.
- `<-` is synonyme with `=`
- `->` is the reversed assignement operator, it assignes the left hand side to the right hand side

Note that `let` creates a new variable, and so it can create anything.
A value, or const reference can be assigned to itself if it's the same.

```rust
3u = 3u;
3u -> 3u;
```

A reference can be assigned to a value. The reference can change type.

```rust
let a = 3i; // a starts as NoType, becomes I
a <- 13u; // ok
16f -> a; // ok
```

## `main` global variable

To make a program, assign an unevaluated expression to the global `main` variable. The expression must either produce an `I` or `null`.

```rust
let main = null? // Our first program! Exit value of 0 because everything is awesome!
```

```rust
let main = 1i? // Oh no everything is not awesome, return value is 1 :/
```

## Types as values

Types are of type `Type` and can be used as values.

```rust
let a = float;

```

The `typeof` prefix gets the type of an expression.

```rust
let a = 1i;
a = typeof a; // I
a = typeof a; // Type
a = typeof a; // Type
```

## Tuples

Tuples are declared using the `(` and `)` operators, and each element must be seperated using the `,` operator.

Trailing `null` values can be added and removed from a tuple without changing it's value or type.
```rust
(1i, 2i, null) <=> (1i, 2i)
```

A tuple's `n`th entry can be accessed using `[n]`.

```rust
(1i, 2i)[0] <=> 1i
(1i, 2i)[2] <=> null
```

The `sizeof` prefix returns the amount of entries before the tuples only contains `Null` type entries.

```rust
sizeof (1i, 2i, 3i); // 3
```

### Tuple Assignement

Tuples are assigned value by value

```rust
let a = ~_;
let b = ~_;
(a, b) = (1u, 2i); // a = 1u, b = 2u, c = 3i
// (a, 3u) = (4u, 5f) // illegal, 3u is not 5f
```

## Variants

Variants hold alternatives. They are build using the `|` operator.

```rust
(3u | 4u) // either 3 or 4
```

The `~` operator inverts a value, the new value contains every possible value that could exist that isn't `~`'s parameter, including other types.
`~` can't be applied to references.

```rust
(3u) | (~3u) // is _
```

Variants can be restricted using the `&` operator.

```rust
(2u | 3u | 4u) & ~2u <=> (3u | 4u)
```

Accessing an entry from Variant of tuples returns a variant of the corresponding element:

```rust
((3u, 4u) | (5u, 6u))[1]; // 4u | 6u
((3u) | (5u, 6u))[1]; // null | 6u
```

### Variant assignement

When assigning to or from a variant, kontext tries to assign to each variation is order:

```rust
let a = 1u;
let b = 2u;
(3u, a) | (4u, b) = (4u, 10u); // assigns b to 10u
```

This allows to do pattern matching:
```rust
let b = ~_;
(3u, b) = (3u, 5u) | (4u, 6u); // assigns b to 5u
```

Assigning a variant to a variant is not supported.


## Arrays

Kontext has no arrays. Tuples and variants are used instead.

Conceptually, an array's size can be retrieved using the `sizeof` keyword.

A dynamic array's general type is a variant of a tuple of each possible size.
However, since a specific variable is one variation of this variant, doing `sizeof` on it will dynamicly return the array's size.
The size is effectively stored in the type.


## Structs and scopes

Structs are groups of variables.

Each scope has an implicit struct, which can be refered to using `this`.

The `emit` keyword allows to return a value from a scope.

```rust
let dog = {
    let woof = ~null;
    emit this;
};

// dog.woof = ~null

let file = {
    let file = open_file my_file;
    emit file;
};
```

Anything after the `emit` keyword becomes part of the destructor which is called when the returned scope is destroyed:

```rust
let file = {
    let file = open_file my_file;
    emit file;
    close_file file; // will be called when file is no longer used
};
```

The `receive` keyword can be used to ask for a value. `receive` returns a prefix which will call the reste of the scope with the parameter.
A `receive` after a `receive` is legal, it returns a prefix returning a prefix.

```rust
let open_file_safe = {
    let file_name = receive;
    let open_mode = receive;
    let file = open_file file_name open_mode;
    emit file;
    close_file file; // will be called when file is no longer used
}?;

let file = open_file_safe! my_file read_write;
```

If receive can't find a value to receive, kontext will emit an error.

The `dispose` ends a variable's life early. If an `emit` or `receive` follows a `emit`, `dispose` has to be called before the variable is destroyed, and will return a `value` or `prefix` depending on the used keyword.

```rust
let write_file_later = {
    let file_name = receive;
    emit file;
    let dest_file_path = receive;
    let error = write file_name dest_file_path;
    emit error
}?;

let file = write_file_later! my_file;
/* to stuff with file */
let error = dispose file my_destination_path; // not calling dispose would trigger an error
```

`recurse` starts the scope again from the top. `break` ends the scope early.

`return x;` is equivalent to `emit x; break;`

```rust
{
    let fibo = 1;
    let prev_fibo = 0;
    let iteration = 0;
    emit {
        (+iteration, let func) = (
            (10, (return fibo;)?) |
            (_, ()?)
        );
        func!;

        let prev_prev_fibo = prev_fibo;
        prev_fibo = fibo;
        fibo = fibo + prev_prev_fibo;

        iteration += 1;
        recurse;
    };
}
```


`reject` allows to reject a value. It must follow a receive. it's good pratice to reject incorrect input.
`reject` is used for _type decay_, which will is the way kontext programs should conventionally handle operator precedence.

```rsut
let times_2 = {
    let number = receive;
    // The usage of ~_ makes sure none of the branches return
    (number, ~_) = (
        (_i,
            emit number * 2;
            break;
        ) |
        (_f,
            emit number * 2.0f
            break;
        )
        (_,
            reject;
            recurse;
        )
    );
}?

times_2! +2; // '+' is a value (see suffixes), so we want to avoid accepting it.
```

When a value is rejected, the `rejected` field is executed if it exists and is not null, and it's return value is used.

```rust
let bob = {
    let ouch = null;
    emit this | (
        let ouch = ~null;
        emit this;
    )?;
};

let alice = {
    receive;
    reject;
}?;

alice! bob; // bob.ouch = ~null;
```

The `_` prefix can be used to refer to the value of a variable in the parent scope. `_` can be chained multiple times.
Note that if the variable isn't shadowed, it can be referred directly.

```
let b = 7;
let c = 2;
let a = {
    // let a = a; // assigns a variable to itself
    let a = _a; // recursive reference, yay!
    let c = b; // refers to outside b
    let d = c; // refers to inside c
    _b = _c; // we can even work with outside variables
};
```

Each `emit` emits a new distinct type, even if the destructor happens to match.
The same emit executed mulitple times returns the same type.

### Variants of structs

Accessing a field from a variant of structs returns a variant of the corresponding field in each struct

```rust
({
    let a = 1u
    let b = 4u;
    emit this;
} | {
    let a = 5u;
    let b = 6u;
    emit this;
}).a; // 1u | 5u
```


### Struct Assignement

When assiging a struct to a struct, matching fields are assigned together. All of the assigned struct's members must be assigned.

```rust
let a = ~_;
{
    let b = a;
    emit this;
} = {
    let b = 3;
    emit this;
} // a is assigned to 3.
```


Variables can be ommited.
```rust
null = {let dog = 3f, emit this;};
```

## The 'token' type

// TODO


# Standard library

The standard library provides most of kontext's funcitonnality and is fully customizable.

## Operators implementation

The standard library distiguishes between _data_ and _operators_.

Both _data_ and _operators_ define the `precedence` field which tells the current precedence of the token.

This precedence is taken from the precendence type linked list.
```
let additive_precedence = {let next_precedence = null; emit this;};
let multiplicative_precedence = {let next_precedence = AdditivePrecedence; emit this;};
let max_precedence = MultiplicativePrecedence;
```

This linked should be extended for specific user operator precedences.

_data_ also has a `data` field containing the exact value.

Standard data types are instanciated the following way:
```rust
<standard_data_type> = {
    let data = /* build in */;
    build = {
        let data = receive;
        let precedence = receive;
        let rejected = {
            let value = receive;
            +value.precedence = (
            (precedence,
                emit value.apply_suffix! this;
            ) |
            (_,
                precedence.next_precedence -> (
                (null,
                    reject;
                ) | (precedence,
                    emit build! data precedence;
                ));
            ));
        }?;
        emit this;
    }?;
    emit build! data max_precedence;
}?;
```

Standard binary operator types are defined in the following way:

```rust
make_binary_operator = {
    let operator_token = receive;
    let operator_precedence = receive;
    let binary_operator_application = receive;
    let unary_operator_application = prefer_receive;

    let build = {
        let precedence = /* <binary operator's precedence> */;
        let apply_suffix = {
            let left_hand_value = receive;
            let right_hand_value = receive;
            right_hand_value.precedence -> (
            (+precedence,
                emit left_hand_value.build! (binary_operator_application left_hand_value.data right_hand_value.data) left_hand_value.precedence;
            ) |
            (_,
                reject;
            ));
        }?
        unary_operator_application -> (
        (~null,
            let rejected = {
                let value = receive;
                emit value.build! (unary_operator_application> value.data) max_precedence;
            }?;
        ), (null));
        emit this;
    }?;
    emit build!;
}?;

let + = make_binary_operator additive_precedence add remove_reference;
let - = make_binary_operator additive_precedence substract negative;
let * = make_binary_operator multiplicative_precedence multiply;
let / = make_binary_operator multiplicative_precedence divide;
let % = make_binary_operator multiplicative_precedence remainder;
```

The same pattern can be used to redefine or define new operators.
It is preferred to edit the functions themselves if new types have to be supported.

TODO:
- `RawIntPrefix`: `"[0-9]+"`, appliable to `IntTypeValue``, UnsignedIntTypeValue` and `FloatTypeValue`, to return the matching templated `NumberConstant`
- `RawFlaotPrefixt` `"[0-9]+.[0-9]+"`, appliable to `FloatTypeValue`, to return the matching templated `NumberConstant<f>`
- `IntTypeValue`: `"i"`
- `UnsignedIntTypeValue`: `"u"`
- `FloatTypeValue`: `"f"`
- `NumberConstant` a templated type instanciated for numbers
  - Decays to `MultiplicationNumberValue`, which can be a parameter to a `MultiplicationOperatorPrefix` that returns a `MultiplicationNumberValue`
  - Decays to `MultiplicationNumberPrefix`, which can be applied on a `MultiplicationOperatorValue`, returning a `MultiplicationOperatorPrefix`
  - Decays to `AdditionNumberValue`, which can be a parameter to an `AdditionOperatorPrefix` that returns an `AdditionNumberValue`
  - Decays to `AdditionNumberPrefix`, which can be applied on an `AdditionOperatorValue`, returning an `AdditionOperatorPrefix`
  - Decays to `ResultNumber`

Example parsing:

`4 + 5 * (6 + 3)`

'4' -> parsed as value
'+' -> parsed as suffix returning a prefix returning a value
'5' -> parsed as value
'*' -> parsed as suffix returning a prefix returning a value
'(' -> parsed as prefix returning a prefix or value (when applied to ')')
'6' -> parsed as value
'+' -> parsed as suffix
'3' -> parsed as value  returning a prefix returning a value
')' -> parsed as suffix returning a value


# Examples

### Strong typing

```rust
let build_trong_type = {
    let value = recieve;
    const let type = typeof value;
    +1 = caridnality type;
    emit this;
}?;

let a = build_strongt_type! 1i;
let b = build_strongt_type! 1f;
// a = b; won't compile
let c = build_strongt_type! 3i;
let d = build_strongt_type! (3i | 2f)

c = a;
(c | b) = a;
(c | b) = 3f;
(c | b).type; // (typeof _i | typeof _f)
d = b; // no problem
d = c; // no problem
d = (c | b); // woops

sizeof (1, 3, 4) // 3
cardinality (1, 3, 4) // 1

(x) | (x, x) | (x, x, x) etc.
(x) == (x, null, null ...)

sizeof arr; // 

arr[7] == (x | null)

compile (1 + A);

```

```rust
prefix (value | expr)
```
